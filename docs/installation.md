# Installation

- [Linux/macOS](#linuxmacos)
- [Windows](#windows)

All downloads are only for 64-bit but you can perform a build on your system so that a binary will be created that is for your system architecture.

**Download:** <https://gitlab.com/boring10/sam/-/releases>

**Supported systems:** Linux, macOS, Windows

> *NOTE: Please be sure to read through the scripts prior to running them on your system to ensure that they are safe. I know that they are not doing anything malicious but it is recommended to check for yourself.*

## Linux/macOS

> *This script has not been fully vetted outside of my personal use. Please use this with caution.*

You can install SAM on Linux/macOS if you are using `BASH` or `ZSH` shells.

```bash
# BASH
wget -qO - "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-linux.sh/raw?ref=main" | bash -
```

```bash
# ZSH
wget -qO - "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-linux.sh/raw?ref=main" | bash -s ".zshrc"
```

Depending on your system, one of the following files will be modified.

- `$HOME/.bashrc`
- `$HOME/.zshrc`
- `$HOME/zsh_profile`

The information below will be appended. This will add the path to SAM so that you don't need to reference it's location when calling it. This also adds an autocomplete feature that only works on Linux/macOS due to using [yargs](https://github.com/yargs/yargs).

```bash
# Begin SAM Path and Completion #
export PATH="$HOME/bin/sam:$PATH"

# yargs command completion script
_sam_yargs_completions()
{
  local reply
  local si=$IFS
  IFS=$'
' reply=($(COMP_CWORD="$((CURRENT-1))" COMP_LINE="$BUFFER" COMP_POINT="$CURSOR" sam --get-yargs-completions "${words[@]}"))
  IFS=$si
  _describe 'values' reply
}
compdef _sam_yargs_completions sam
# End SAM Path and Completion   #
```

## Windows

> *This script has not been fully vetted outside of my personal use. Please use this with caution.*

```bash
Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-windows.ps1/raw?ref=main" -UseBasicParsing)
```

The following content will be appended to your `$profile`. The `$env:PATH` will be updated to include the directory for the binary.

```bash
# Begin SAM Path #
$env:Path += ";$HOME/bin/sam"
# End SAM Path   #
```

At this time, there is no tab autocompletion capability for PowerShell.

