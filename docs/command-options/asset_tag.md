# asset_tag

- [Alias](#alias)
- [hardware patch byid](#hardware-patch-byid)
- [hardware post byid](#hardware-post-byid)
- [hardware post create](#hardware-post-create)
- [Usage](#usage)

## Alias

- `--asset_tag`
- `--at`
- `--a_t`

## hardware patch byid

- **Description:** Update the unique asset tag for the asset.
- **Type:** string

## hardware post byid

- **Description:** Unique asset tag for the asset.
- **Type:** string

## hardware post create

- **Description:** Unique asset tag for the asset.
- **Type:** string

## Usage

Asset tags are inserted as strings since alpha-numeric characters can be used.

```bash
--asset_tag "23412431"
```
