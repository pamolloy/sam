# api_throttle

- [Alias](#alias)
- [config profiles create](#config-profiles-create)
- [config profiles update](#config-profiles-update)
- [Usage](#usage)

> ***NOTE: This command is currently a placeholder for future versions where throttling will be active to reduce the chance of overloading the Snipe-IT server instance.***

## Alias

- `--api_throttle`
- `--a_throt`

## config profiles create

- **Description:** The API throttle limit.
- **Default:** 120
- **Type:** number

## config profiles update

- **Description:** The API throttle limit.
- **Type:** number

## Usage

If `--api_throttle` is omitted then the default value is used for that command. Only applies if a default is set for that command.

```bash
--api_throttle 100
```
