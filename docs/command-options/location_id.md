# location_id

## Alias

- `--location_id`
- `--locid`
- `--loc_id`
- `--locationid`

## accessories patch byid

- **Description:** Update the ID of the location the accessory is assigned to.
- **Type:** number

## accessories post create

- **Description:** ID of the location the accessory is assigned to.
- **Type:** number

## components patch byid

- **Description:** Update the ID of the location the component is assigned to.
- **Type:** number

## components post create

- **Description:** ID of the location the component is assigned to.
- **Type:** number

## consumables patch byid

- **Description:** Update the ID of the location the consumable is assigned to.
- **Type:** number

## consumables post create

- **Description:** ID of the location the consumable is assigned to.
- **Type:** number

## departments patch byid

- **Description:** Update the ID of the location the department is assigned to.
- **Type:** number

## departments post create

- **Description:** ID of the location the department is assigned to.
- **Type:** number

## hardware get search

- **Description:** Optionally restrict the hardware results to this location ID.
- **Type:** number

## hardware post checking byid

- **Description:** The location ID of the asset.
- **Type:** number

## users patch byid

- **Description:** Update the location ID of the user.
- **Type:** number

## users post create

- **Description:** The location ID of the user.
- **Type:** number

## Usage

```bash
--location_id 3
```

