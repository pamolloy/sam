# category_type

- [Alias](#alias)
- [categories patch byid](#categories-patch-byid)
- [categories post create](#category-post-create)
- [Usage](#usage)

## Alias

- `--category_type`
- `--cattype`
- `--cat_type`
- `--categorytype`

## categories patch byid

- **Description:** Type of category.
- **Choices:** asset, accessory, consumable, component, license
- **Type:** string

## categories post create

- **Description:** Type of category.
- **Required:** true,
- **Choices:** asset, accessory, consumable, component, license
- **Type:** string

## Usage

```bash
--category_type "asset"
```
