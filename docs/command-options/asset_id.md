# asset_id

- [Alias](#alias)
- [maintenances get search](#maintenances-get-search)
- [maintenances post create](#maintenances-post-create)
- [Usage](#usage)

## Alias

- `--asset_id`
- `--ai`
- `--a_i`

## maintenances get search

- **Description:** Asset ID of the asset you'd like to return maintenances for.
- **Type:** number

## maintenances post create

- **Description:** Unique asset ID for the asset.
- **Required:** true
- **Type:** number

## Usage

```bash
--asset_id 38
```
