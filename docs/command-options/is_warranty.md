# is_warranty

## Alias

- `--is_warranty`
- `--i_w`
- `--is_warranty`

## maintenances post create

- **Description:** States whether the maintenance is covered under warranty or not.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--is_warranty

# Set to false
--is_warranty false
# or
--no-is_warranty
```

