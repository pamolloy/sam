# expected_checkin

## Alias

- `--expected_checkin`
- `--exp_chk`
- `--expectedcheckin`

## hardware patch byid

> 2021-03-13: At this time, this does not notify the user. I don't know if this is something that Snipe-IT is planning to implement or not.

- **Description:** Update the date that the asset is expected to be checked back in.
- **Type:** string

## hardware post byid

- **Description:** Date that the asset is expected to be checked back in.
- **Type:** string

## Usage

```bash
--expected_checkin "2021-02-19"
```

