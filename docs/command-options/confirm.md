# confirm

## Alias

- `--confirm`
- `-c`
- `--cfm`

## config profiles delete all

- **Description:** Confirm that you would like to delete ALL of the profiles without being prompted.
- **Type:** boolean

## config profiles delete byname

- **Description:** Confirm that you would like to delete the profile without being prompted.
- **Type:** boolean

## Usage

```bash
# Sets to true
--confirm
```

