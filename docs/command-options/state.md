# state

## Alias

- `--state`
- `--st`

## locations patch byid

- **Description:** Update the state.
- **Type:** string

## locations post create

- **Description:** The state.
- **Type:** string

## users patch byid

- **Description:** Update the state.
- **Type:** string

## users post create

- **Description:** The state.
- **Type:** string

## Usage

```bash
--state "FL"
```

