# sort

## Alias

- `--sort`
- `--so`

## accessories get checkedout byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## accessories get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## categories get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## companies get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## components get assets byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## components get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## consumables get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## departments get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## hardware get audit due

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## hardware get audit overdue

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## hardware get licenses byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## hardware get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## licenses get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## locations get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## maintenances get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## manufacturers get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## models get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## statuslabels get assetlist byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## statuslabels get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## users get accessories byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## users get assets byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## users get licenses byid

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## users get search

- **Description:** Specify the field name you wish to sort by.
- **Default:** id
- **Type:** string

## Usage

```bash
--sort "asset_tag"
```

