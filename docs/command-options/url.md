# url

## Alias

- `--url`
- `-u`

## manufacturers patch byid

- **Description:** Update the URL for the manufacturer.
- **Type:** string

## manufacturers post create

- **Description:** URL for the manufacturer.
- **Type:** string

## config profiles create

- **Description:** The full URL to your instance of Snipe-IT.
- **DemandOption:** true
- **Type:** string

## config profiles update

- **Description:** The full URL to your instance of Snipe-IT.
- **Type:** string

## Usage

```bash
--url "https://snipe-it-domain-instance.com"
```

