# jobtitle

## Alias

- `--jobtitle`
- `--jt`

## users patch byid

- **Description:** Update the job title for the user.
- **Type:** string

## users post create

- **Description:** The job title for the user.
- **Type:** string

## Usage

```bash
--jobtitle "Manager"
```

