# assigned_user

- [Alias](#alias)
- [hardware post checkout byid](#hardware-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--assigned_user`
- `--assg_usr`
- `--assigneduser`

## hardware post checkout byid

- **Description:** The ID of the user to assign the asset out to.
- **Type:** number

## Usage

```bash
--assigned_user 12
```

