# serial

## Alias

- `--serial`
- `-s`

## components patch byid

- **Description:** Update the serial number of the component.
- **Type:** string

## components post create

- **Description:** The serial number of the component.
- **Type:** string

## hardware patch byid

- **Description:** Update the serial number of the hardware.
- **Type:** string

## hardware post create

- **Description:** The serial number of the hardware.
- **Type:** string

## licenses patch byid

- **Description:** Update the serial number (product key) of the licenses.
- **Type:** string

## licenses post create

- **Description:** Serial number (product key) of the licenses.
- **Type:** string

## Usage

```bash
--serial "2345153hj1324"
```

