# purchase_date

## Alias

- `--purchase_date`
- `--purdt`
- `--pur_dt`
- `--purchasedate`

## accessories patch byid

- **Description:** Update the date accessory was purchased.
- **Type:** string

## accessories post create

- **Description:** Date accessory was purchased.
- **Type:** string

## components patch byid

- **Description:** Update the date component was purchased.
- **Type:** string

## components post create

- **Description:** Date component was purchased.
- **Type:** string

## consumables patch byid

- **Description:** Update the date consumable was purchased.
- **Type:** string

## consumables post create

- **Description:** Date consumable was purchased.
- **Type:** string

## hardware patch byid

- **Description:** Update the date that the hardware was purchased.
- **Type:** string

## hardware post create

- **Description:** Date that the hardware was purchased.
- **Type:** string

## licenses patch byid

- **Description:** Update the date of license purchase.
- **Type:** string

## licenses post create

- **Description:** Date of license purchase.
- **Type:** string

## Usage

```bash
--purchase_date "2021-02-19"
```

