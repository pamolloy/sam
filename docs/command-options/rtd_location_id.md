# rtd_location_id

## Alias

- `--rtd_location_id`
- `--rtdlocid`
- `--rtd_loc_id`
- `--rtdlocationid`

## hardware patch byid

- **Description:** Update the ID that corresponds to the location where the hardware is usually stored when not checked out.
- **Type:** number

## hardware post create

- **Description:** The ID that corresponds to the location where there hardware is usually stored when not checked out.
- **Type:** number

## Usage

```bash
--rtd_location_id 32
```

