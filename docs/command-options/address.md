# address

- [Alias](#alias)
- [locations patch byid](#locations-patch-byid)
- [locations post create](#locations-post-create)
- [users patch byid](#users-patch-byid)
- [users poost create](#users-post-create)
- [Usage](#usage)

## Alias

- `--address`
- `--adr`
- `--addr`
- `--add`

## locations patch byid

- **Description:** Update the address for the location.
- **Type:** string

## locations post create

- **Description:** Address for the location.
- **Type:** string

## users patch byid

- **Description:** Update the address for the user.
- **Type:** string

## users post create

- **Description:** Address for the user.
- **Type:** string

## Usage

```bash
--address "101 Somewhere St."
```
