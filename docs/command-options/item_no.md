# item_no

## Alias

- `--item_no`
- `--i_n`
- `--in`
- `--itemno`

## consumables patch byid

- **Description:** Update the item number of the component.
- **Type:** string

## consumables post create

- **Description:** The item number of the component.
- **Type:** string

## Usage

```bash
--item_no "123abc"
```

