# order_number

## Alias

- `--order_number`
- `--ordno`
- `--ord_num`
- `--ordernumber`

## accessories get search

- **Description:** Return only accessories associated with a specific order number.
- **Type:** string

## accessories patch byid

- **Description:** Update the order number for the accessory.
- **Type:** string

## accessories post create

- **Description:** Order number for the accessory.
- **Type:** string

## components patch byid

- **Description:** Update the order number for the component.
- **Type:** string

## components post create

- **Description:** Order number for the component.
- **Type:** string

## consumables get search

- **Description:** Return only consumables associated with a specific order number.
- **Type:** string

## consumables patch byid

- **Description:** Update the order number for the consumable.
- **Type:** string

## consumables post create

- **Description:** Order number for the consumable.
- **Type:** string

## hardware patch byid

- **Description:** Update the order number for the hardware.
- **Type:** string

## hardware post byid

- **Description:** Order number for the hardware.
- **Type:** string

## hardware post create

- **Description:** Order number for the hardware.
- **Type:** string

## licenses get search

- **Description:** Return only licenses associated with a specific order number.
- **Type:** string

## licenses patch byid

- **Description:** Update the order number of the license purchase.
- **Type:** string

## licenses post create

- **Description:** Order number of the license purchase.
- **Type:** string

## Usage

```bash
--order_number "abc123462"
```

