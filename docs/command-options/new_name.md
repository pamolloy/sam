# new_name

## Alias

- `--new_name`
- `--nn`
- `--newname`

## config profiles update

> All profile names have spaces removed and are converted to lowercase automatically.

- **Description:** New name of the profile.
- **Type:** string

## Usage

```bash
--new_name "My Profile Name" # -> myprofilename
```

