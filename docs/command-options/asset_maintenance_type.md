# asset_maintenance_type

- [Alias](#alias)
- [hardware post checkout byid](#hardware-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--asset_maintenance_type`
- `--amt`
- `--assetmaintenancetype`

## hardware post checkout byid

- **Description:** Type of maintenance that is needed or was performed.
- **Choices:** calibration, hardware, support, maintenance, pat test, repair, software, support, upgrade
- **Required:** true
- **Type:** string

## Usage

```bash
--asset_maintenance_type "repair"
```
