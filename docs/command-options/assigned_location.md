# assigned_location

- [Alias](#alias)
- [hardware post checkout byid](#hardware-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--assigned_location`
- `--assg_loc`
- `--assignedlocation`

## hardware post checkout byid

- **Description:** The ID of the location to assign the asset out to.
- **Type:** number

## Usage

```bash
--assigned_location 123
```
