# update_location

## Alias

- `--update_location`
- `--updt_loc`
- `--updtloc`
- `--updatelocation`

## hardware post audit bytag

- **Description:** Whether to update the location or not.
- **Type:** boolean

## Usage

```bash
# Set to true
--update_location

# Set to false
--update_location false
```

