# zip

## Alias

- `--zip`
- `--z`

## locations patch byid

- **Description:** Update the zip code.
- **Type:** string

## locations post create

- **Description:** The zip code.
- **Type:** string

## users patch byid

- **Description:** Update the zip code.
- **Type:** string

## users post create

- **Description:** The zip code.
- **Type:** string

## Usage

```bash
--zip "123456"
```

