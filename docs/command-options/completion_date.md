# completion_date

## Alias

- `--completion_date`
- `--comp_date`
- `--completiondate`

## maintenances post create

- **Description:** Date of maintenance completion.
- **Type:** string

## Usage

```bash
--completion_date "2021-04-13"
```

