# purchase_order

## Alias

- `--purchase_order`
- `--pur_order`
- `--purchaseorder`

## licenses patch byid

- **Description:** Update the purchase order number.
- **Type:** string

## licenses post create

- **Description:** Purchase order number.
- **Type:** string

## Usage

```bash
--purchase_order "AB1231465"
```

