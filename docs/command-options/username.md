# username

## Alias

- `--username`

## users patch byid

- **Description:** Update the username for the user. This must be unique across all users.
- **Type:** string

## users post create

- **Description:** The username for the user. This must be unique across all users.
- **Required:** true
- **Type:** string

## Usage

```bash
--username "example.username"
```

