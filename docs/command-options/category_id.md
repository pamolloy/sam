# category_id

- [Alias](#alias)
- [accessories patch byid](#accessories-patch-byid)
- [accessories post create](#accessories-post-create)
- [components patch byid](#components-patch-byid)
- [components post create](#components-post-create)
- [consumables get search](#consumables-get-search)
- [consumables patch byid](#consumables-patch-byid)
- [consumables post create](#consumables-post-create)
- [hardware get search](#hardware-get-search)
- [licenses patch byid](#licenses-patch-byid)
- [licenses post create](#licenses-post-create)
- [models patch byid](#models-patch-byid)
- [models post create](#models-post-create)
- [Usage](#usage)

## Alias

- `--category_id`
- `--catid`
- `--cat_id`
- `--categoryid`

## accessories patch byid

- **Description:** Update the ID of the category the accessory belongs to.
- **Type:** number

## accessories post create

- **Description:** ID of the category the accessory belongs to.
- **Required:** true
- **Type:** number

## components patch byid

- **Description:** Update the ID of the category the component belongs to.
- **Type:** number

## components post create

- **Description:** ID of the category the component belongs to.
- **Required:** true
- **Type:** number

## consumables get search

- **Description:** Category ID to filter by.
- **Type:** number

## consumables patch byid

- **Description:** Update the ID of the category the consumable belongs to.
- **Type:** number

## consumables post create

- **Description:** ID of the category the consumable belongs to.
- **Required:** true
- **Type:** number

## hardware get search

- **Description:** Optionally restrict the hardware results to this category.
- **Type:** number

## licenses patch byid

- **Description:** Update the ID of the category the license belongs to.
- **Type:** number

## licenses post create

- **Description:** ID of the category the license belongs to.
- **Required:** true
- **Type:** number

## models patch byid

- **Description:** Update the ID of the category the model belongs to.
- **Type:** number

## models post create

- **Description:** ID of the category the model belongs to.
- **Required:** true
- **Type:** number

## Usage

```bash
--category_id 3
```
