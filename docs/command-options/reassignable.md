# reassignable

## Alias

- `--reassignable`

## licenses patch byid

- **Description:** Is the license reassignable to another asset or user.
- **Type:** boolean

## licenses post create

- **Description:** Is the license reassignable to another asset or user.
- **Type:** boolean

## Usage

```bash
# Set to true
--reasssignable

# Set to false
--reasssignable false
# or
--no-reasssignable
```

