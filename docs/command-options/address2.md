# address2

- [Alias](#alias)
- [locations patch byid](#locations-patch-byid)
- [locations post create](#locations-post-create)
- [Usage](#usage)

## Alias

- `--address2`
- `--adr2`
- `--addr2`
- `--add2`

## locations patch byid

- **Description:** Update the apartment, condo, floor number, etc.
- **Type:** string

## locations post create

- **Description:** Apartment, condo, floor number, etc.
- **Type:** string

## Usage

```bash
--address2 "101 Somewhere St."
```
