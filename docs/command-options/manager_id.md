# manager_id

## Alias

- `--manager_id`
- `--mgr_id`
- `--mgrid`
- `--managerid`

## departments patch byid

- **Description:** Update the ID of the user that will be listed as the manager for the department.
- **Type:** number

## departments post create

- **Description:** The ID of the user that will be listed as the manager for the department.
- **Type:** number

## locations patch byid

- **Description:** Update the ID of the user that will be listed as the manager for the location.
- **Type:** number

## locations post create

- **Description:** The ID of the user that will be listed as the manager for the location.
- **Type:** number

## users patch byid

- **Description:** Update the ID of the user that will be listed as the manager for the location.
- **Type:** number

## users post create

- **Description:** The ID of the user that will be listed as the manager for the location.
- **Type:** number

## Usage

```bash
--manager_id 13
```

