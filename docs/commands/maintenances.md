# Maintenances

- [Delete](#delete)
    - [maintenances delete byid](#maintenances-delete-byid)
- [Get](#get)
    - [maintenances get byid](#maintenances-get-byid)
    - [maintenances get search](#maintenances-get-search)
<!-- - [Patch](#patch) -->
<!--     - [maintenances patch byid](#maintenances-patch-byid) -->
- [Post](#post)
    - [maintenances post create](#maintenances-post-create)

## Delete

```bash
sam maintenances delete byid 6
> Successfully deleted the maintenance with the ID of "6" from the asset ID of "2" (asset tag: 1448840485).
```

## Get

### maintenances get byid

```bash
sam maintenances get byid 5 \
    --fields "id,asset.id,asset.asset_tag,title,model.name"
> id: 5
> asset.id: 2
> asset.asset_tag: 1448840485
> model.name: Macbook Pro 13" 345807932057468
> title: Something
```

### maintenances get search

```bash
sam maintenances get search \
    --fields "id,asset.id,asset.asset_tag,title,model.name" \
    --asset_id 1
> id: 9
> asset.id: 1
> asset.asset_tag: 610028948
> model.name: Macbook Pro 13" 345807932057468
> title: Something
```

<!-- ## Patch -->

<!-- ### maintenances patch byid -->

## Post

### maintenances post create

```bash
sam maintenances post create \
    --asset_id 3 \
    --asset_maintenance_type "repair" \
    --start_date "2021-04-24" \
    --supplier_id 2 \
    --title "Something went wrong..."
> Successfully created the maintenance "Something went wrong..." with the ID "1" for the asset with the ID of "3".
```
