# Hardware

- [Delete](#delete)
    - [hardware delete byid](#hardware-delete-byid)
    - [hardware delete bytag](#hardware-delete-bytag)
- [Get](#get)
    - [hardware get byid](#hardware-get-byid)
    - [hardware get byserial](#hardware-get-byserial)
    - [hardware get bytag](#hardware-get-bytag)
    - [hardware get licenses byid](#hardware-get-licenses-byid)
    - [hardware get licenses bytag](#hardware-get-licenses-bytag)
    - [hardware get search](#hardware-get-search)
- [Patch](#patch)
    - [hardware patch byid](#hardware-patch-byid)
    - [hardware patch bytag](#hardware-patch-bytag)
- [Post](#post)
    - [hardware post audit bytag](#hardware-post-audit-bytag)
    - [hardware post checkin byid](#hardware-post-checkin-byid)
    - [hardware post checkout byid](#hardware-post-checkout-byid)
    - [hardware post create](#hardware-post-create)

## Delete

### hardware delete byid

> 2021-03-15: Snipe-IT does not pass any information about the device back when deleting an asset. The only information that is passed back is if it was successfull or if the asset does not exist.
> 2021-08-29: Delete now makes at least 2 API calls. Once to ensure that the asset exists and to obtain information on the asset. The second is to perform the deletion.

```bash
sam hardware delete byid 2
> Successfully deleted the asset with the ID of 2.
```

### hardware delete bytag

> 2021-08-29: Delete now makes at least 2 API calls. Once to ensure that the asset exists and to obtain information on the asset, specifically the ID of the asset since the API does not allow for deleting by the asset tag. The second is to perform the deletion.

```bash
sam hardware delete bytag "test176701"
> The asset was deleted successfully on Sun Aug 29, 2021 7:26AM.

sam hardware delete bytag "test176700" \
  --fields "id,asset_tag,name"
> The asset was deleted successfully on Sun Aug 29, 2021 7:28AM.
> id: 178149
> name: 176700
> asset_tag: test176700
```

## Get

### hardware get byid

You can limit the fields of the command by using the [`--fields`](../command-options/fields.md) option.

```bash
sam hardware get byid 1 \
  --fields "id,name,asset_tag,serial,model.name"
> id: 1
> name: 
> asset_tag: 539302473
> serial: ed779147-f547-3d57-9739-c03087717cfa
> model.name: Macbook Pro 13"
```

See below for an example of not restricting the fields.

```bash
sam hardware get byid 1
> id: 1
> name: 
> asset_tag: 539302473
> serial: ed779147-f547-3d57-9739-c03087717cfa
> model.id: 1
> model.name: Macbook Pro 13"
> model_number: 2449844266824057
> eol.date: 2023-11-20
> eol.formatted: Mon Nov 20, 2023
> status_label.id: 1
> status_label.name: Ready to Deploy
> status_label.status_type: deployable
> status_label.status_meta: deployed
> category.id: 1
> category.name: Laptops
> manufacturer.id: 1
> manufacturer.name: Apple
> supplier.id: 1
> supplier.name: Nienow, Leannon and Lindgren
> notes: Created by DB seeder
> order_number: 14009131
> company: null
> location.id: 2
> location.name: Grahamburgh
> rtd_location.id: 8
> rtd_location.name: East Justicebury
> image: https://develop.snipeitapp.com/uploads/models/mbp.jpg
> assigned_to.id: 5
> assigned_to.username: madalyn52
> assigned_to.name: Guy Marquardt
> assigned_to.first_name: Guy
> assigned_to.last_name: Marquardt
> assigned_to.employee_number: 33047
> assigned_to.type: user
> warranty_months: null
> warranty_expires: null
> created_at.datetime: 2021-03-14 21:00:02
> created_at.formatted: Sun Mar 14, 2021 9:00PM
> updated_at.datetime: 2021-03-14 21:00:29
> updated_at.formatted: Sun Mar 14, 2021 9:00PM
> last_audit_date: null
> next_audit_date: null
> deleted_at: null
> purchase_date.date: 2020-11-20
> purchase_date.formatted: Fri Nov 20, 2020
> last_checkout: null
> expected_checkin: null
> purchase_cost: 1068.29
> checkin_counter: 0
> checkout_counter: 0
> requests_counter: 0
> user_can_checkout: false
> custom_fields.RAM.field: _snipeit_ram_3
> custom_fields.RAM.value: null
> custom_fields.RAM.field_format: ANY
> custom_fields.CPU.field: _snipeit_cpu_4
> custom_fields.CPU.value: null
> custom_fields.CPU.field_format: ANY
> custom_fields.MAC Address.field: _snipeit_mac_address_5
> custom_fields.MAC Address.value: null
> custom_fields.MAC Address.field_format: regex:/^([0-9a-fA-F]{2}[:-]){5}[0-9a-fA-F]{2}$/
> available_actions.checkout: true
> available_actions.checkin: true
> available_actions.clone: true
> available_actions.restore: false
> available_actions.update: true
> available_actions.delete: false
```

### hardware get byserial

Unless you restrict your Snipe-IT instance to prevent duplicate serial numbers, this command may return more than one result. This option is located under the `admin settings -> General Settings`.

```bash
sam hardware get byserial "ed779147-f547-3d57-9739-c03087717cfa" \
  --fields "id,name,asset_tag,category.name,supplier.name"
> id: 1
> name: 
> asset_tag: 539302473
> category.name: Laptops
> supplier.name: Nienow, Leannon and Lindgren
```

Example with a duplicate serial number.

```bash
sam hardware get byserial "ed779147-f547-3d57-9739-c03087717cfa" \
  --fields "id,name,asset_tag,category.name,supplier.name"
> id: 1
> name: 
> asset_tag: 539302473
> category.name: Laptops
> supplier.name: Nienow, Leannon and Lindgren
> 
> id: 4
> name: 
> asset_tag: 89312329
> category.name: Laptops
> supplier.name: Nienow, Leannon and Lindgren
```

### hardware get bytag

```bash
sam hardware get bytag 89312329 \
  --fields "id,name,asset_tag,serial"
> id: 4
> name: 
> asset_tag: 89312329
> serial: 6f282c1f-3141-3745-bc20-a610f6937137
```

### hardware get licenses byid

```bash
sam hardware get licenses byid 1 \
  --fields "id,name,manufacturer.name,purchase_date.date,purchase_date.formatted,purchase_cost,category.name"
> id: 2
> name: Acrobat
> manufacturer.name: Adobe
> purchase_date.date: 2020-04-17
> purchase_date.formatted: Fri Apr 17, 2020
> purchase_cost: 29.99
> category.name: Graphics Software
```

### hardware get licenses bytag

```bash
sam hardware get licenses bytag "126342316" \
  --fields "id,name"
> id: 4
> name: Office
```

### hardware get search

```bash
sam hardware get search --limit 3 --fields id,name,asset_tag,serial,model.name,model.id --model_id 1 --output json
> id: 1
> name: 
> asset_tag: 1099778831
> serial: c961ec3a-096f-3f1f-8f2a-df4bbe6f1b9a
> model.id: 1
> model.name: Macbook Pro 13"
> 
> id: 2
> name: 
> asset_tag: 1065047588
> serial: b6e6dca1-0a7f-3d58-bfc3-994fd48a650c
> model.id: 1
> model.name: Macbook Pro 13"
> 
> id: 3
> name: 
> asset_tag: 1489573149
> serial: 89c3215e-2912-3883-bc05-2d5e4df0868a
> model.id: 1
> model.name: Macbook Pro 13"

sam hardware get search --limit 2 --fields id,name,asset_tag,serial,model.name,model.id --model_id 3
> id: 1151
> name: 
> asset_tag: 1358241781
> serial: ee03b8e5-ef0e-3f18-a793-174dd818b71b
> model.id: 3
> model.name: Surface
> 
> id: 1152
> name: 
> asset_tag: 850452437
> serial: abd709a5-062e-3d43-bb85-0cc96f133f33
> model.id: 3
> model.name: Surface

sam hardware get search --limit 5 --offset 2 --fields "id,asset_tag,serial,custom_fields.MAC Address.value" --output csv 
> id,asset_tag,custom_fields.MAC Address.value,serial
> 9,625792392,,3a62d00c-4496-3616-8942-64ab8c338772
> 10,1054946061,,f768889b-55a0-36c9-ba06-63c622505809
> 11,730813030,,7f397700-07e8-3202-be3a-befbcc88832b
> 12,1453012424,,beadb248-8578-3395-9659-06de4a56c299
> 13,589565762,,18e45976-f57f-383d-aadd-74769b069d11


# This can take awhile with larger databases. For reference, retrieving 175,000 assets took approximately 25 minutes. This does provide you with much more granular information that you can't get by performing the download with the custom export option.
sam hardware get search "Laptops" --all --output csv --export_to_file "/home/[username]/Desktop/laptops.csv"
> Retrieved: 500 of 1235
> Retrieved: 1000 of 1235
> Retrieved: 1235 of 1235
```

## Patch

### hardware patch byid

```bash
sam hardware patch byid 1 \
  --fields "id,notes"
> id: 1
> notes: Created by DB seeder

sam hardware patch byid 1 \
  --notes "A new note for the asset"
> Successfully updated the asset with the ID "1" (asset tag: 539302473)

sam hardware get byid 1 \
  --fields "id,notes"
> id: 1
> notes: A new note for the asset
```

### hardware patch bytag

```bash
sam hardware patch bytag "126342316" \
  --name "Amazing new name"
> Successfully updated the asset with the ID ""
```

## Post

### hardware post audit bytag

```bash
sam hardware post audit bytag "7284088" \
  --location_id 1 \
  --next_audit_date "2021-04-01" \
  --notes "audited"
> Successfully audited the asset with the asset tag "7284088" (next audit: Thu Apr 01, 2021 12:00AM).
```

### hardware post checkin byid

```bash
sam hardware post checkin byid 1
> Successfully checked in the asset with the asset tag "539302473".
```

### hardware post checkout byid

```bash
sam hardware post checkout byid 1 \
  --checkout_to_type "user" \
  --assigned_user 2 \
  --expected_checkin "2021-03-21"
> Successfully checked out the asset with the asset tag "539302473".

sam hardware get byid 1 \
  --fields "id,name,asset_tag,last_checkout.datetime,expected_checkin.date"
> id: 1
> name: 
> asset_tag: 539302473
> last_checkout.datetime: 2021-03-15 11:11:17
> expected_checkin.date: 2021-03-21
```

### hardware post create

```bash
sam hardware post create \
  --name "Craig Smith" \
  --serial "l1234dfc" \
  --purchase_date "2021-01-01" \
  --purchase_cost "300.00" \
  --company_id 4 \
  --order_number "5623431df" \
  --status_id 2 \
  --supplier_id 3 \
  --model_id 4 \
  --rtd_location_id 3 \
  --_snipeit_mac_address_5 "12:34:56:aa:bb:cc"
> Successfully create an asset with the ID "1442" and asset tag "ASC46896".

sam hardware get byid 1442 \
  --fields "id,name.serial,purchase_date.date,purchase_cost,company.id,company.name,order_numer,status.id,status.name,supplier.id,supplier.name,model.id,model.name,rtd_location.id,rtd_location.name,custom_fields.MAC Address.field,custom_fields.MAC Address.value"
> id: 1442
> name: Craig Smith
> serial: l1234dfc
> model.id: 4
> model.name: XPS 13
> supplier.id: 3
> supplier.name: Hammes Group
> company.id: 4
> company.name: Denesik-Kuhlman
> location.id: 3
> location.name: Daphneside
> rtd_location.id: 3
> rtd_location.name: Daphneside
> purchase_date.date: 2021-01-01
> purchase_cost: 300.00
> custom_fields.MAC Address.field: _snipeit_mac_address_5
> custom_fields.MAC Address.value: 12:34:56:aa:bb:cc
```
