# Config - local SAM settings

Manage the application configuration and settings.

- [Profiles](#profiles)
    - [config profiles create](#config-profiles-create)
    - [config profiles delete byname](#config-profiles-delete-byname)
    - [config profiles delete all](#config-profiles-delete-all)
    - [config profiles list active](#config-profiles-list-active)
    - [config profiles list all](#config-profiles-list-all)
    - [config profiles list byname](#config-profiles-list-byname)
    - [config profiles update byname](#config-profiles-update-byname)
- [Release](#release)
    - [config release latest](#config-release-latest)
- [Settings](#settings)
    - [config settings checkforupdates](#config-settings-checkforupdates)

## Profiles

Manage your API profiles for Snipe-IT.

All profile names will have spaces removed and be converted to all lowercase characters. The name must be unique if you have multiple different profiles that you are managing.

### config profiles create

All profile names will have spacing removed and they will be converted to all lowercase characters.

```bash
sam config profiles create \
  --name "Profile Name" \
  --active \
  --url "https://snipe-it-instance.com" \
  --api_key "21ljck28c754tugc2457ky568cxk653ycx2lk4y7t82c48ty56c8yk2457cxl254y74c57"
> The profile "profilename" has been created. This profile is now the currently active profile

sam config profiles create \
  --name "Another Profile" \
  --active false \
  --url "https://example.com" \
  --auto_asset_tag false \
  --api_key "3khg68c3597fx3kl568fx2k8t6538l6uyt8x2ky86rfxhg4935xz2t4j7zy8x568lkuyt3x98"
> The profile "anotherprofile" has been created.
```

### config profiles delete byname

```bash
sam config profiles delete byname "profilename"
Delete the profile "profilename"? (Y/n): y
> The profile "profilename" has been deleted.

# You can also confirm the delete while issuing the command.
sam config profiles list byname "profilename" --confirm
> The profile "profilename" has been deleted.

sam config profiles list all
> anotherprofile
>   active: false
>   url: https://example.com
>   auto_asset_tag: false
>   api_key: 3khg68c359[truncated]
>   api_throttle: 120
>   api_version: v1
```

### config profiles delete all

```bash
sam config profiles delete all
Delete all profiles? (Y/n): y
> All profiles have been deleted.

# you can also confirm the delete while issuing the command.
sam config profiles delete all --confirm
> All profiles have been deleted.
```

### config profiles list active

```bash
sam config profiles list active
> profilename
>   active: true
>   url: https://snipe-it-instance.com
>   api_key: 21ljck28c754tugc2457ky568cxk653ycx2lk4y7t82c48ty56c8yk2457cxl254y74c57
>   api_throttle: 120
>   api_version: v1
>   auto_asset_tag: true
```

### config profiles list all

```bash
sam config profiles list all
> profilename
>   active: true
>   url: https://snipe-it-instance.com
>   api_key: 21ljck28c7[truncated]
>   api_throttle: 120
>   api_version: v1
>   auto_asset_tag: true
> 
> anotherprofile
>   active: false
>   url: https://example.com
>   auto_asset_tag: false
>   api_key: 3khg68c359[truncated]
>   api_throttle: 120
>   api_version: v1
```

### config profiles list byname

```bash
sam config profiles list byname "anotherprofile"
> active: false
> url: https://example.com
> auto_asset_tag: false
> api_key: 3khg68c3597fx3kl568fx2k8t6538l6uyt8x2ky86rfxhg4935xz2t4j7zy8x568lkuyt3x98
> api_throttle: 120
> api_version: v1

sam config profiles list byname "Profile Name"
> active: true
> url: https://snipe-it-instance.com
> api_key: 21ljck28c754tugc2457ky568cxk653ycx2lk4y7t82c48ty56c8yk2457cxl254y74c57
> api_throttle: 120
> api_version: v1
> auto_asset_tag: true
```

### config profiles update byname

```bash
sam config profiles update byname "anotherprofile" \
  --active
> The profile "anotherprofile" has been updated.
```

## Release

### config release latest

```bash
sam config release latest
> # 1.0.0 (2021-03-14)
> 
> 
> ### Features
> 
> * **init release:** initial release ([234lk124](https://gitlab.com/boring10/sam/commit/39cf671b654ef849f35135c58556b586e5b4fc05))
```

## Settings

### config settings checkforupdates

```bash
# By default this is enabled already
sam config settings checkforupdates --enabled
> The configuration setting "check_for_updates" has been set to true

# Disable the automatic check for updates
sam config settings checkforupdates --enabled false
> The configuration setting "check_for_updates" has been set to false
```
