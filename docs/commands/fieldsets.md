# fieldset_id

## Alias

- `--fieldset_id`
- `--flds_id`
- `--f_id`
- `--fieldsetid`

## models patch byid

- **Description:** Update the ID of the fieldset for the model.
- **Type:** number

## models post create

- **Description:** ID of the fieldset for the model.
- **Type:** number

## Usage

```bash
--fieldset_id 12
```

