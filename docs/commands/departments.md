# Departments

- [Delete](#delete)
    - [departments delete byid](#departments-delete-byid)
- [Get](#get)
    - [departments get byid](#departments-get-byid)
    - [departments get search](#departments-get-search)
- [Patch](#patch)
    - [departments patch byid](#departments-patch-byid)
- [Post](#post)
    - [departments post create](#departments-post-create)

## Delete

> 2021-03-25: Snipe-IT does not pass any information about the device back when deleting a department. The only information that is passed back is if it was successfull or if the department does not exist.

```bash
sam departments delete byid 9
> Successfully deleted the department with the ID of "9".
```

## Get

### departments get byid

```bash
sam departments get byid 9 
> id: 9
> name: Development Dept.
> image: null
> company: null
> manager.id: 6
> manager.name: Efren Kris
> manager.first_name: Efren
> manager.last_name: Kris
> location: null
> users_count: 
> created_at.datetime: 2021-04-09 11:14:40
> created_at.formatted: Fri Apr 09, 2021 11:14AM
> updated_at.datetime: 2021-04-09 11:15:32
> updated_at.formatted: Fri Apr 09, 2021 11:15AM
> available_actions.update: true
> available_actions.delete: true
```

### departments get search

```bash
sam departments get search \
    --fields "id,name" \
    --limit 3 \
    --offset 2 \
    --sort id \
    --order asc \
    --output json
> {
>   '1': { id: '3', name: 'Marketing' },
>   '2': { id: '4', name: 'Client Services' },
>   '3': { id: '5', name: 'Product Management' }
> }
```

## Patch

### departments patch byid

```bash
sam departments patch byid 9 \
    --manager_id 6
> Successfully updated the department with the ID "9" named "Development Dept.".
```

## Post

### departments post create

```bash
sam departments post create \
    --name "Development Dept."
> Successfully created a department with the ID "9" named "Development Dept.".
```
