const ErrorHandler = require('./errorHandler')
const { appendToFile } = require('./log')
const DEBUG = true

/**
 * @namespace Error
 * Global error handling
 *
 * @param {Object} err Original error message.
 * @param {Object} opts
 * @param {string | Array} opts.message Custom error message from command.
 * @param {boolean} opts.exit Whether to stop the process or allow the program to continue.
 *
 * @returns {null}
 */
module.exports = (err, opts) => {
  opts = Object.assign({ exit: false }, opts)

  let error = { ...err }

  // Error for being unable to establish a connection.
  if (err.code === 'EAI_AGAIN') {
    const obj = {
      errno: err.errno,
      code: err.code,
      syscall: err.syscall,
      currentUrl: err.request._currentUrl,
    }

    const message = `A temporary failure in name resolution occurred. Unable to establish a connection to "${obj.currentUrl}".`

    error = new ErrorHandler(message)

    obj.message = error.message

    if (DEBUG) {
      console.error(obj)
      appendToFile(`[ERROR]: ${JSON.stringify(obj)}`)
    } else {
      console.error(obj.message)
      appendToFile(`[ERROR]: ${obj.message}`)
    }
  }

  if (JSON.parse(err.data).status === 'error') {
    const data = JSON.parse(err.data)

    const obj = {
      statusCode: err.status,
      statusText: err.statusText,
      statusType: data.status,
      currentUrl: err.request.res.responseUrl,
      message: data.messages,
    }

    if (DEBUG) {
      console.error(obj)
      appendToFile(`[ERROR]: ${JSON.stringify(obj)}`)
    } else {
      console.error(obj.message)
      appendToFile(`[ERROR]: ${obj.message}`)
    }
  }

  if (opts.exit) {
    process.exit(1)
  }
}
