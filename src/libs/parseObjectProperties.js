/**
 * @namespace ParseObjectProperties
 */
const replaceHtmlCodes = require('./replaceHtmlCodes')

/**
 * @memberof ParseObjectProperties
 * @description Get properties recursively from an object.
 * @author https://stackoverflow.com/a/53620876
 *
 * @param {Object} dataObj Object that contains key/value pairs.
 *
 * @returns {array} Array of an object keys that are returned.
 */
const parseObjectProperties = (dataObj) => {
  const isObject = (x) => {
    if (
      typeof x === 'object' &&
      !Array.isArray(x) &&
      x !== undefined &&
      x !== null
    ) {
      return true
    }

    return false
  }

  const appendProperty = (a, b) => {
    if (a) {
      return `${a}.${b}`
    }

    return b
  }

  const objProperties = (obj = {}, head = '') => {
    return Object.entries(obj).reduce((product, [key, value]) => {
      let fullpath = appendProperty(head, key)

      if (isObject(value)) {
        return product.concat(objProperties(value, fullpath))
      } else {
        return product.concat(({} = { [fullpath]: replaceHtmlCodes(value) }))
      }
    }, [])
  }

  return objProperties(dataObj)
}

module.exports = parseObjectProperties
