const deepClone = require('./deepClone')

describe('deepClone', () => {
  let originalObject

  beforeEach(() => {
    originalObject = {
      key1: 'value1',
      key2: 'value2',
      nestedObj1: {
        key1: 'value1',
        nestedObj2: {
          key1: 'value1',
          func: function () {
            return 'test'
          },
        },
      },
    }
  })

  it('should return an object', () => {
    let newObject = deepClone(originalObject)

    expect(typeof newObject).toBe('object')
  })

  it('should contain a key key/value pair', () => {
    let newObject = deepClone(originalObject)
    newObject.newValue = 'test'

    expect(newObject.hasOwnProperty('newValue')).toBeTruthy()
  })

  it('should not contain key in original object that was added to the new object', () => {
    let newObject = deepClone(originalObject)
    newObject.newValue = 'test'

    expect(originalObject.hasOwnProperty('newValue')).toBeFalsy()
  })

  it('should contain a nested object', () => {
    let newObject = deepClone(originalObject)

    expect(newObject.nestedObj1.hasOwnProperty('key1')).toBeTruthy()
  })

  it('should contain a function', () => {
    let newObject = deepClone(originalObject)

    expect(typeof newObject.nestedObj1.nestedObj2.func).toBe('function')
  })
})
