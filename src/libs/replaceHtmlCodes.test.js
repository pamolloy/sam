const replaceHtmlCodes = require('./replaceHtmlCodes')

const htmlCodes = [
  ['&quot;', '"'],
  ['&#039;', "'"],
]

describe('replaceHtmlCodes()', () => {
  let str

  beforeEach(() => {
    str = ''
  })

  it('should be a string', () => {
    str = 'test'

    expect(typeof replaceHtmlCodes(str)).toBe('string')
  })

  it('should not modify a string that does not include the character codes', () => {
    str = 'test'

    expect(replaceHtmlCodes(str)).toBe(str)
  })

  htmlCodes.forEach((code) => {
    it(`should replace ( ${code[0]} ) with ( ${code[1]} )`, () => {
      str = `Some ${code[0]} that should be replaced.`

      expect(replaceHtmlCodes(str)).toBe(
        `Some ${code[1]} that should be replaced.`
      )
    })
  })
})
