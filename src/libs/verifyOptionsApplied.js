/**
 * @namespace VerifyOptionsApplied
 */
const log = require('./log')

/**
 * Compare the data that was sent with the data that was received.
 *
 * @param {Object} sent Data object that was sent to the server.
 * @param {Object} received Data that was received from the server.
 *
 * @param {array}
 */
const verifyOptionsApplied = (sent, received) => {
  let array = []
  let keys = Object.keys(sent)

  keys.forEach((key) => {
    let msg
    let sentData = sent[key].toString()
    let receivedData = (function () {
      if (received[key]) {
        return received[key].toString()
      }

      return ''
    })()

    if (sentData !== receivedData) {
      console.log(key)
      console.log(sent[key])
      if (key === 'pruchase_date') {
        if (sentData !== receivedData.split(' ')[0]) {
          msg = `Failed to update the field "--${key} ${sent[key]}"`

          log.error(msg)
          array.push(msg)
        } else {
          msg = `Failed to update the field "--${key} ${sent[key]}"`

          log.error(msg)
          array.push(msg)
        }
      }
    }
  })

  return array
}

module.exports = verifyOptionsApplied
