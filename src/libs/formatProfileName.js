/**
 * @namespace FormatProfileName
 */

/**
 * @memberof FormatProfileName
 * @description Remove spaces from the profile name and convert the string to all lowercase.
 *
 * @param {string} name Raw profile name.
 *
 * @returns {string} String without spaces and all lowercase characters.
 */
const formatProfileName = (str) => {
  return str.replace(/ /gi, '').toLowerCase()
}

module.exports = formatProfileName
