/**
 * @namespace RegExOptions
 */

/**
 * @memberof RegExOptions
 * @description Convert MAC address to have colons.
 *
 * @param {string} str String to convert into mac address.
 *
 * @returns {str}
 */
const macAddress = (str) => {
  const reg = /([0-9A-Fa-f]{2})1:/
  // const reg = /(.*):$1/

  console.log(str.replace(reg, 'g'))
  return str
}

module.exports = { macAddress }
