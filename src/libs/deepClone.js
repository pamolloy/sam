/**
 * @namespace DeepClone
 */

/**
 * @memberof DeepClone
 * @description
 * Perform a deep clone of an object.
 *
 * https://stackoverflow.com/a/65164143/12066662
 *
 * @param {object} obj
 *
 * @returns {object}
 */
const deepClone = (obj) => {
  let clone = {}

  Object.keys(obj).map((prop) => {
    if (Array.isArray(obj[prop])) {
      clone[prop] = [].concat(obj[prop])
    } else if (typeof obj[prop] === 'object') {
      clone[prop] = deepClone(obj[prop])
    } else {
      clone[prop] = obj[prop]
    }
  })

  return clone
}

module.exports = deepClone
