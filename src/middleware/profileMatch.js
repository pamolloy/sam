/**
 * @namespace ProfileMatch
 */
const formatProfileName = require('../libs/formatProfileName')
var profiles = require(require('../libs/files').fileLocations().profiles)

/**
 * @memberof ProfileMatch
 * @description Check for any profile that has a matching name.
 *
 * @param {Object} argv
 *
 * @returns {void}
 */
const profileMatch = (argv) => {
  const profileName = formatProfileName(argv.options.name)

  const filter = Object.keys(profiles).filter((item) => item === profileName)

  /**
   * @private
   * @description Message for when a profile name already exists.
   *
   * @param {string} name Name of the profile.
   *
   * @returns {string} Message to be returned.
   */
  function profileExists(name) {
    return `A profile with the name "${name}" already exists.`
  }

  /**
   * @private
   * @description Message for when a profile name is not found.
   *
   * @param {string} name Name of the profile.
   *
   * @returns {string} Message to be returned.
   */
  function profileNotFound(name) {
    return `A profile with the name "${name}" was not found.`
  }

  if (
    argv.section === 'config' &&
    argv.method === 'profiles' &&
    argv.commands[0] === 'create'
  ) {
    if (profiles.hasOwnProperty(profileName)) {
      argv.error = profileExists(profileName)
    }

    return argv
  }

  if (!filter.length >= 1) {
    argv.error = profileNotFound(profileName)

    return argv
  }

  argv.profileMatch = {}
  argv.profileMatch[profileName] = profiles[profileName]

  return argv
}

module.exports = profileMatch
