const yargs = require('yargs')

const commands = [
  'hardware',
  'hardware delete',
  'hardware delete byid',
  'hardware get',
  'hardware get audit',
  'hardware get audit due',
  'hardware get audit overdue',
  'hardware get byid',
  'hardware get byserial',
  'hardware get bytag',
  'hardware get licenses',
  'hardware get licenses byid',
  'hardware get search',
  'hardware patch',
  'hardware patch byid',
  'hardware post',
  'hardware post audit',
  'hardware post audit bytag',
  'hardware post checkin',
  'hardware post checkin byid',
  'hardware post checkout',
  'hardware post checkin',
  'hardware post checkin byid',
  'hardware post create',
]

describe('Command help for hardware', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
