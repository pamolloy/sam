const yargs = require('yargs')

const commands = [
  'categories',
  'categories delete',
  'categories delete byid',
  'categories get',
  'categories get byid',
  'categories get search',
  'categories patch',
  'categories patch byid',
  'categories post',
  'categories post create',
]

describe('Command help for categories', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
