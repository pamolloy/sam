const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'create'
exports.description = 'Create a new category.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('category_type', commandOptions.option('category_type'))
    .option('checkin_email', commandOptions.option('checkin_email'))
    .option('eula_text', commandOptions.option('eula_text'))
    .option('name', commandOptions.option('name'))
    .option('require_acceptance', commandOptions.option('require_acceptance'))
    .option('use_default_eula', commandOptions.option('use_default_eula'))
    .coerce('category_type', (opt) => opt.toLowerCase())
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully created a category with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
