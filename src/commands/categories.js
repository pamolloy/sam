exports.command = 'categories <method>'
exports.description = 'Manage your categories.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('categories')
}
