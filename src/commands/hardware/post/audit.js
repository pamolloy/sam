exports.command = 'audit <command>'
exports.description = 'Audit the asset.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./audit')
}
