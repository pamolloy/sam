const CommandOptions = require('../../../../libs/commandOptions')
const generateConfig = require('../../../../middleware/generateConfig')
const hardwareHandlers = require('../../../../yargsHandlers/hardwareHandlers')

exports.command = 'bytag <asset_tag>'
exports.description = 'Checkin an asset by the asset asset tag.'
exports.builder = function(yargs) {
    const commandOptions = new CommandOptions(yargs.getContext().commands)

    return yargs
        .option('fields', commandOptions.option('fields'))
        .option('location_id', commandOptions.option('location_id'))
        .option('name', commandOptions.option('name'))
        .option('notes', commandOptions.option('notes'))
        .option('status_id', commandOptions.option('status_id'))
        .middleware([generateConfig])
}
exports.handler = async function(argv) {
    hardwareHandlers.postCheckinBytag(argv)
}
