const CommandOptions = require('../../../../libs/commandOptions')
const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const hardwareHandlers = require('../../../../yargsHandlers/hardwareHandlers')

exports.command = 'byid <id>'
exports.description = 'Checkin an asset by the asset ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('fields', commandOptions.option('fields'))
    .option('location_id', commandOptions.option('location_id'))
    .option('name', commandOptions.option('name'))
    .option('notes', commandOptions.option('notes'))
    .strict((enabled = true))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  hardwareHandlers.postCheckinByid(argv)
}
