const axios = require('axios')

const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const log = require('../../../../libs/log')

exports.command = 'bypivotid <id>'
exports.description =
  'Check in a consumable using the assigned pivot ID. This is different from the consumable ID and the user ID. The ID has the fieldname "assigned_pivot_id".'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.idBeforeLastCommand])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(`Successfully checked in the accessory.`)
  } catch (err) {
    log.error(err)
  }
}
