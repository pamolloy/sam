exports.command = 'checkout <command>'
exports.description = 'Checkout a consumable to a user.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkout')
}
