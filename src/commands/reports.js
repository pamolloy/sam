exports.command = 'reports <method>'
exports.description = 'Manage your reports.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods', {
      exclude: function (path) {
        if (/delete\.js$/.test(path)) {
          return true
        } else if (/patch\.js$/.test(path)) {
          return true
        } else if (/post\.js$/.test(path)) {
          return true
        } else {
          return false
        }
      },
    })
    .commandDir('reports')
}
