const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Update an existing component.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('category_id', commandOptions.option('category_id'))
    .option('company_id', commandOptions.option('company_id'))
    .option('location_id', commandOptions.option('location_id'))
    .option('min_amt', commandOptions.option('min_amt'))
    .option('name', commandOptions.option('name'))
    .option('order_number', commandOptions.option('order_number'))
    .option('purchase_cost', commandOptions.option('purchase_cost'))
    .option('purchase_date', commandOptions.option('purchase_date'))
    .option('qty', commandOptions.option('qty'))
    .option('serial', commandOptions.option('serial'))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully updated the component with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
