/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for asset_maintenance_type.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const asset_maintenance_type = (commands) => {
  const { section, method } = commands

  if (section === 'maintenances') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  let choices = [
    'calibration',
    'hardware support',
    'maintenance',
    'pat test',
    'repair',
    'software support',
    'upgrade',
  ]

  return {
    maintenances: {
      post: {
        create: {
          alias: alias('asset_maintenance_type'),
          description: 'Type of maintenance that is needed or was performed.',
          required: true,
          choices,
          coerce: (obj) => obj.toLowerCase(),
          type: 'string',
        },
      },
    },
  }
}

module.exports = asset_maintenance_type
