/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for min_amt.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const min_amt = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('min_amt'),
          description:
            'Update the minimum quantity that should be available before an alert gets triggered',
          hidden: true,
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('min_amt'),
          description:
            'The minimum quantity that should be available before an alert gets triggered',
          hidden: true,
          type: 'number',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('min_amt'),
          description:
            'Update the minimum quantity that should be available before an alert gets triggered',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('min_amt'),
          description:
            'The minimum quantity that should be available before an alert gets triggered',
          type: 'number',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('min_amt'),
          description:
            'Update the minimum quantity that should be available before an alert gets triggered',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('min_amt'),
          description:
            'The minimum quantity that should be available before an alert gets triggered',
          type: 'number',
        },
      },
    },
  }
}

module.exports = min_amt
