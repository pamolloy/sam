/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for start_date.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const start_date = (commands) => {
  const { section, method } = commands

  if (section === 'maintenances') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    maintenances: {
      post: {
        create: {
          alias: alias('start_date'),
          description: 'Date the the maintenance was started or logged.',
          required: true,
          type: 'string',
        },
      },
    },
  }
}

module.exports = start_date
