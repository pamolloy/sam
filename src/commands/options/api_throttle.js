/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for api_throttle.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const api_throttle = (commands) => {
  const { section, method } = commands

  if (section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    config: {
      profiles: {
        create: {
          alias: alias('api_throttle'),
          description: 'The API throttle limit.',
          default: 120,
          type: 'number',
        },
        update: {
          alias: alias('api_throttle'),
          description: 'The API throttle limit.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = api_throttle
