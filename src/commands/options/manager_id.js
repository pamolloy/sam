/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for manager_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const manager_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'departments' ||
    section === 'locations' ||
    section === 'users'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    departments: {
      patch: {
        byid: {
          alias: alias('manager_id'),
          description:
            'Update the ID of the user that will be listed as the manager for the department.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manager_id'),
          description:
            'The ID of the user that will be listed as the manager for the department.',
          type: 'number',
        },
      },
    },
    locations: {
      patch: {
        byid: {
          alias: alias('manager_id'),
          description:
            'Update the ID of the user that will be listed as the manager for the location.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manager_id'),
          description:
            'The ID of the user that will be listed as the manager for the location.',
          type: 'number',
        },
      },
    },
    users: {
      patch: {
        byid: {
          alias: alias('manager_id'),
          description:
            'Update the ID of the user that will be listed as the manager for the user.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manager_id'),
          description:
            'The ID of the user that will be listed as the manager for the user.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = manager_id
