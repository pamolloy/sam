/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for two_factor_enrolled.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const two_factor_enrolled = (commands) => {
  const { section, method } = commands

  if (section === 'users') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    users: {
      patch: {
        byid: {
          alias: alias('two_factor_enrolled'),
          description: 'Whether the user is two factor enrolled or not.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('two_factor_enrolled'),
          description: 'Whether the user is two factor enrolled or not.',
          default: false,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = two_factor_enrolled
