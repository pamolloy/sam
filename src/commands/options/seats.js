/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for seats.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const seats = (commands) => {
  const { section, method } = commands

  if (section === 'licenses') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    licenses: {
      patch: {
        byid: {
          alias: alias('seats'),
          description: 'Update the number of license seats owned.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('seats'),
          description: 'Number of license seats owned.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = seats
