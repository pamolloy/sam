/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for requestable.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const requestable = (commands) => {
  const { section, method } = commands

  if (
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'models'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    consumables: {
      patch: {
        byid: {
          alias: alias('requestable'),
          description:
            'Update whether or not the consumable is requestable by users with the permission to request consumables.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('requestable'),
          description:
            'Whether or not the consumable is requestable by users with the permission to request consumables.',
          type: 'boolean',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('requestable'),
          description:
            'Update whether or not the hardware is requestable by users with the permission to request hardware.',
          type: 'boolean',
        },
        bytag: {
          alias: alias('requestable'),
          description:
            'Update whether or not the hardware is requestable by users with the permission to request hardware.',
          type: 'boolean',
        },
        byserial: {
          alias: alias('requestable'),
          description:
            'Update whether or not the hardware is requestable by users with the permission to request hardware.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('requestable'),
          description:
            'Whether or not the hardware is requestable by users with the permission to request hardware.',
          type: 'boolean',
        },
      },
    },
    // models: {
    //   patch: {
    //     byid: {
    //       alias: alias('requestable'),
    //       description:
    //         'Update whether or not the model is requestable by users with the permission to request hardware.',
    //       coerce: (opt) => (opt ? 1 : 0),
    //       type: 'boolean',
    //     },
    //   },
    //   post: {
    //     create: {
    //       alias: alias('requestable'),
    //       description:
    //         'Whether or not the model is requestable by users with the permission to request hardware.',
    //       coerce: (opt) => (opt ? 1 : 0),
    //       type: 'boolean',
    //     },
    //   },
    // },
  }
}

module.exports = requestable
