/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for name.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const name = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'categories' ||
    section === 'companies' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'departments' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'locations' ||
    section === 'manufacturers' ||
    section === 'models' ||
    section === 'statuslabels' ||
    section === 'config'
  ) {
    let { command1 } = commands

    if (command1 === 'checkin' || command1 === 'checkout') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the accessory',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the accessory',
          required: true,
          type: 'string',
        },
      },
    },
    categories: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the category',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the category',
          required: true,
          type: 'string',
        },
      },
    },
    companies: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the company.',
          required: true,
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the company.',
          required: true,
          type: 'string',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the component.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the component.',
          required: true,
          type: 'string',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the consumables.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the consumables.',
          required: true,
          type: 'string',
        },
      },
    },
    departments: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the department.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the department.',
          required: true,
          type: 'string',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the asset.',
          type: 'string',
        },
        bytag: {
          alias: alias('name'),
          description: 'Update the name of the asset.',
          type: 'string',
        },
        byserial: {
          alias: alias('name'),
          description: 'Update the name of the asset.',
          type: 'string',
        },
      },
      post: {
        checkin: {
          byid: {
            alias: alias('name'),
            description: 'Name of the asset.',
            type: 'string',
          },
          bytag: {
            alias: alias('name'),
            description: 'Name of the asset.',
            type: 'string',
          },
        },
        checkout: {
          byid: {
            alias: alias('name'),
            description: 'Name of the asset.',
            type: 'string',
          },
          bytag: {
            alias: alias('name'),
            description: 'Name of the asset.',
            type: 'string',
          },
        },
        create: {
          alias: alias('name'),
          description: 'Name of the asset.',
          type: 'string',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the license.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the license.',
          required: true,
          type: 'string',
        },
      },
    },
    locations: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the location.',
          required: true,
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the location.',
          required: true,
          type: 'string',
        },
      },
    },
    manufacturers: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the manufacturer.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the manufacturer.',
          required: true,
          type: 'string',
        },
      },
    },
    models: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the model.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the model.',
          required: true,
          type: 'string',
        },
      },
    },
    statuslabels: {
      patch: {
        byid: {
          alias: alias('name'),
          description: 'Update the name of the status label.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('name'),
          description: 'Name of the status label.',
          required: true,
          type: 'string',
        },
      },
    },
    config: {
      profiles: {
        create: {
          alias: alias('name'),
          description: 'Name of the profile',
          required: true,
          type: 'string',
        },
        update: {
          alias: alias('name'),
          description: 'Name of the profile',
          required: true,
          type: 'string',
        },
      },
    },
  }
}

module.exports = name
