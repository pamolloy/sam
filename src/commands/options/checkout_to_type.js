/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for checkout_to_type.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const checkout_to_type = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    if (command1 === 'checkout') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      post: {
        checkout: {
          byid: {
            alias: alias('checkout_to_type'),
            description: 'Checkout the asset to an asset, location, or user.',
            choices: ['asset', 'location', 'user'],
            default: 'user',
            type: 'string',
          },
          bytag: {
            alias: alias('checkout_to_type'),
            description: 'Checkout the asset to an asset, location, or user.',
            choices: ['asset', 'location', 'user'],
            default: 'user',
            type: 'string',
          },
        },
      },
    },
  }
}

module.exports = checkout_to_type
