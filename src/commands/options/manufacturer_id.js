/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for manufacturer_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const manufacturer_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'models'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('manufacturer_id'),
          description: 'Update the ID of the manufacturer for this accessory.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manufacturer_id'),
          description: 'ID of the manufacturer for this accessory.',
          type: 'number',
        },
      },
    },
    consumables: {
      get: {
        search: {
          alias: alias('manufacturer_id'),
          description: 'Manufacturer ID to filter by.',
          type: 'number',
        },
      },
      patch: {
        byid: {
          alias: alias('manufacturer_id'),
          description: 'Update the ID of the manufacturer for this consumable.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manufacturer_id'),
          description: 'ID of the manufacturer for this consumable.',
          type: 'number',
        },
      },
    },
    hardware: {
      get: {
        search: {
          alias: alias('manufacturer_id'),
          description:
            'Optionally restrict the hardware results to this manufacturer ID.',
          type: 'number',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('manufacturer_id'),
          description: 'Update the ID of the manufacturer of the license.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manufacturer_id'),
          description: 'ID of the manufacturer of the license.',
          required: true,
          type: 'number',
        },
      },
    },
    models: {
      patch: {
        byid: {
          alias: alias('manufacturer_id'),
          description: 'Update the ID of the manufacturer of the model.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('manufacturer_id'),
          description: 'ID of the manufacturer of the model.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = manufacturer_id
