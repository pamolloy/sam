/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for expand.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const expand = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'consumables' ||
    section === 'licenses' ||
    section === 'users'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  let obj = {
    alias: alias('expand'),
    description:
      'Whether to include detailed information on categories, etc (true) or just the text name (false).',
    default: true,
    type: 'boolean',
  }

  return {
    accessories: {
      get: {
        search: obj,
      },
    },
    consumables: {
      get: {
        search: obj,
      },
    },
    licenses: {
      get: {
        search: obj,
      },
    },
    users: {
      get: {
        search: obj,
      },
    },
  }
}

module.exports = expand
