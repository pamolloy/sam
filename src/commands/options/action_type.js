/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for action_type.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const action_type = (commands) => {
  const { section, method } = commands

  if (section === 'reports') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    reports: {
      get: {
        activity: {
          alias: alias('action_type'),
          description:
            'The action type you\'e querying against. Example values here are: "add seats", "checkin from", "checkout", "update".',
          type: 'string',
        },
      },
    },
  }
}

module.exports = action_type
