/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for supplier_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const supplier_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'maintenances'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('supplier_id'),
          description: 'Update the ID of the supplier for this accessory.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('supplier_id'),
          description: 'ID of the supplier for this accessory.',
          type: 'number',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('supplier_id'),
          description: 'Update the ID that corresponds with the supplier.',
          type: 'number',
        },
        bytag: {
          alias: alias('supplier_id'),
          description: 'Update the ID that corresponds with the supplier.',
          type: 'number',
        },
        byserial: {
          alias: alias('supplier_id'),
          description: 'Update the ID that corresponds with the supplier.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('supplier_id'),
          description: 'The ID that corresponds with the supplier.',
          type: 'number',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('supplier_id'),
          description: 'Update the ID of the license supplier.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('supplier_id'),
          description: 'ID of the license supplier.',
          type: 'number',
        },
      },
    },
    maintenances: {
      post: {
        create: {
          alias: alias('supplier_id'),
          description: 'ID of the maintenance supplier.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = supplier_id
