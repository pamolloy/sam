/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for purchase_date.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const purchase_date = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'licenses'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date accessory was purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_date'),
          description: 'Date accessory was purchased.',
          type: 'string',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date component was purchased.',
          type: 'string',
        },
      },
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date component was purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_date'),
          description: 'Date component was purchased.',
          type: 'string',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date consumable was purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_date'),
          description: 'Date consumable was purchased.',
          type: 'string',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date that the hardware was purchased.',
          type: 'string',
        },
        bytag: {
          alias: alias('purchase_date'),
          description: 'Update the date that the hardware was purchased.',
          type: 'string',
        },
        byserial: {
          alias: alias('purchase_date'),
          description: 'Update the date that the hardware was purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_date'),
          description: 'Date that the hardware was purchased.',
          type: 'string',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('purchase_date'),
          description: 'Update the date of license purchase.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_date'),
          description: 'Date of license purchase.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = purchase_date
