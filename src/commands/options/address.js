/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for address.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const address = (commands) => {
  const { section, method } = commands

  if (section === 'locations' || section === 'users') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    locations: {
      patch: {
        byid: {
          alias: alias('address'),
          description: 'Update the address for the location.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('address'),
          description: 'Address for the location.',
          type: 'string',
        },
      },
    },
    users: {
      patch: {
        byid: {
          alias: alias('address'),
          description: 'Update the address for the user.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('address'),
          description: 'Address for the user.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = address
