/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for url.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const url = (commands) => {
  const { section, method } = commands

  if (section === 'manufacturers' || section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    manufacturers: {
      patch: {
        byid: {
          alias: alias('url'),
          description: 'Update the URL for the manufacturer.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('url'),
          description: 'URL for the manufacturer.',
          type: 'string',
        },
      },
    },
    config: {
      profiles: {
        create: {
          alias: alias('url'),
          description: 'The full URL to your instance of Snipe-IT.',
          demandOption: true,
          type: 'string',
        },
        update: {
          alias: alias('url'),
          description: 'The full URL to your instance of Snipe-IT.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = url
