/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for order_number.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const order_number = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'licenses'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      get: {
        search: {
          alias: alias('order_number'),
          description:
            'Return only accessories associated with a specific order number.',
          type: 'string',
        },
      },
      patch: {
        byid: {
          alias: alias('order_number'),
          description: 'Update the order number for the accessory.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('order_number'),
          description: 'Order number for the accessory.',
          type: 'string',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('order_number'),
          description: 'Update the order number for the component.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('order_number'),
          description: 'Order number for the component.',
          type: 'string',
        },
      },
    },
    consumables: {
      get: {
        search: {
          alias: alias('order_number'),
          description:
            'Return only accessories associated with a specific order number.',
          type: 'string',
        },
      },
      patch: {
        byid: {
          alias: alias('order_number'),
          description: 'Update the order number for the consumable.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('order_number'),
          description: 'Order number for the consumable.',
          type: 'string',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('order_number'),
          description: 'Update the order number for the hardware.',
          type: 'string',
        },
        bytag: {
          alias: alias('order_number'),
          description: 'Update the order number for the hardware.',
          type: 'string',
        },
        byserial: {
          alias: alias('order_number'),
          description: 'Update the order number for the hardware.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('order_number'),
          description: 'Order number for the hardware.',
          type: 'string',
        },
      },
    },
    licenses: {
      get: {
        search: {
          alias: alias('order_number'),
          description:
            'Return only licenses associated with a specific order number.',
          type: 'string',
        },
      },
      patch: {
        byid: {
          alias: alias('order_number'),
          description: 'Update the order number of the license purchase.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('order_number'),
          description: 'Order number of the license purchase.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = order_number
