/**
 * @namespace CommandOptions
 */

/**
 * @memberof CommandOptions
 * @description Command option for custom_fieldname.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const custom_fieldname = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  let description =
    "To include custom fields, you would include the database field name in the payload. Note that this is NOT the human-readable name, but you can get the field name by checking the 'Custom Fields' page and looking for the 'DB Field' column."

  return {
    hardware: {
      get: {
        search: {
          description,
          type: 'string',
        },
      },
      patch: {
        byid: {
          description,
          type: 'string',
        },
        bytag: {
          description,
          type: 'string',
        },
        byserial: {
          description,
          type: 'string',
        },
      },
      post: {
        create: {
          description,
          type: 'string',
        },
      },
    },
  }
}

module.exports = custom_fieldname
