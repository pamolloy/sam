/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for fieldset_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const fieldset_id = (commands) => {
  const { section, method } = commands

  if (section === 'models') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    models: {
      patch: {
        byid: {
          alias: alias('fieldset_id'),
          description: 'Update the ID of the fieldset for the model.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('fieldset_id'),
          description: 'ID of the fieldset for the model.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = fieldset_id
