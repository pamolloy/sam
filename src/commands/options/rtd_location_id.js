/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for rtd_location_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const rtd_location_id = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      patch: {
        byid: {
          alias: alias('rtd_location_id'),
          description:
            'Update the ID that corresponds to the location where the hardware is usually stored when not checked out.',
          type: 'number',
        },
        bytag: {
          alias: alias('rtd_location_id'),
          description:
            'Update the ID that corresponds to the location where the hardware is usually stored when not checked out.',
          type: 'number',
        },
        byserial: {
          alias: alias('rtd_location_id'),
          description:
            'Update the ID that corresponds to the location where the hardware is usually stored when not checked out.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('rtd_location_id'),
          description:
            'The ID that corresponds to the location where there hardware is usually stored when not checked out.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = rtd_location_id
