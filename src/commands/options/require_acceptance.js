/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for require_acceptance.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const require_acceptance = (commands) => {
  const { section, method } = commands

  if (section === 'categories') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    categories: {
      patch: {
        byid: {
          alias: alias('require_acceptance'),
          description:
            'Require the user to confirm acceptance of the item in this category.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('require_acceptance'),
          description:
            'Require the user to confirm acceptance of the item in this category.',
          default: false,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = require_acceptance
