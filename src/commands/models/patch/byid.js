const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description =
  'Partially update a model, passing only the fields you want to modify.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return (
    yargs
      .options('category_id', commandOptions.option('category_id'))
      .options('eol', commandOptions.option('eol'))
      .options('fieldset_id', commandOptions.option('fieldset_id'))
      .options('manufacturer_id', commandOptions.option('manufacturer_id'))
      .options('model_number', commandOptions.option('model_number'))
      .options('name', commandOptions.option('name'))
      .options('notes', commandOptions.option('notes'))
      // .options('requestable', commandOptions.option('requestable'))
      .middleware([generateConfig, appendToConfigUrl.id])
  )
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    let response = await axios(argv.config)
    let data = response.data
    const payload = data.payload

    log.response(
      `Successfully updated the model with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    console.log(err)
  }
}
