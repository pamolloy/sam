exports.command = 'models <method>'
exports.description = 'Manage your models.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('models')
}
