exports.command = 'checkedout <command>'
exports.description = 'Get the users that the accessory is checked out to.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkedout')
}
