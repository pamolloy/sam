const CommandOptions = require('../../../../libs/commandOptions')
const formatProfileName = require('../../../../libs/formatProfileName')
const log = require('../../../../libs/log')
const profileMatch = require('../../../../middleware/profileMatch')
const writeFile = require('../../../../libs/writeFile')
const profiles = require(require('../../../../libs/files').fileLocations()
  .profiles)

exports.command = 'byname <name>'
exports.description = 'Delete an existing profile by name.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('confirm', commandOptions.option('confirm'))
    .middleware([profileMatch])
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const name = formatProfileName(argv.options.name)
  delete argv.options.name

  let confirm = argv.options.confirm
  if (!confirm) {
    let prompt = require('prompt-sync')({ sigint: true })
    let response

    do {
      response = prompt(`Delete the profile "${name}"? (Y/n): `).toLowerCase()
    } while (response !== 'y' && response !== 'n')

    if (response === 'y') {
      confirm = true
    }
  }

  if (confirm) {
    delete profiles[name]

    writeFile.profiles(profiles)
    log.response(`The profile "${name}" has been deleted.`)
  } else {
    log.response(`The profile "${name}" was not deleted.`)
  }
}
