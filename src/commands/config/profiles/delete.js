exports.command = 'delete <command>'
exports.description = 'Delete existing profile for a Snipe-IT instance.'
exports.builder = function (yargs) {
  return yargs.demandCommand(1).commandDir('./delete')
}
