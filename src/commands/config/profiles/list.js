exports.command = 'list <command>'
exports.description = 'List existing profiles.'
exports.builder = function (yargs) {
  return yargs.demandCommand(1).commandDir('./list')
}
