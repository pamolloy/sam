const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const reportsHandlers = require('../../../yargsHandlers/reportsHandlers')

exports.command = 'activity [search]'
exports.description = 'Search for the assets that you would like to retrieve.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('action_type', commandOptions.option('action_type'))
    .option('all', commandOptions.option('all'))
    .option('export_to_file', commandOptions.option('export_to_file'))
    .option('fields', commandOptions.option('fields'))
    .option('item_id', commandOptions.option('item_id'))
    .option('item_type', commandOptions.option('item_type'))
    .option('limit', commandOptions.option('limit'))
    .option('offset', commandOptions.option('offset'))
    .option('output', commandOptions.option('output'))
    .option('target_id', commandOptions.option('target_id'))
    .option('target_type', commandOptions.option('target_type'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.inOrder])
}
exports.handler = async function (argv) {
  reportsHandlers.getActivity(argv)
}
