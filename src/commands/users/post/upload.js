exports.command = 'upload <command>'
exports.description = 'Upload a file to a user.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./upload')
}
