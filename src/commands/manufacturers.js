exports.command = 'manufacturers <method>'
exports.description = 'Manage your manufacturers.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('manufacturers')
}
