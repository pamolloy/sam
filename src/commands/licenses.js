exports.command = 'licenses <method>'
exports.description = 'Manage your licenses.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('licenses')
}
