const axios = require('axios')

const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Delete a specific maintenance by the ID.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully deleted the maintenance with the ID of "${payload.id}" from the asset ID of "${payload.asset.id}" (asset tag: ${payload.asset.asset_tag}).`
    )
  } catch (error) {
    log.error(error.response.data)
  }
}
